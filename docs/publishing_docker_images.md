# Publishing Docker images

| Status | Date     | History     |
|--------|-------------|------------------|
| Draft  | 12-Dec-2023 | initial version  |

[[_TOC_]]

## Introduction

In this article we will provide best practices for publishing Docker Images in the open (i.e. publicly available) and provide legal documentation recommendations.

If you are publishing artifacts, you are responsible for providing information about the content, especially about the licenses of 3rd party content. The legal obligations of the content must be observed in all forms of which the content is available.

See also the [Eclipse Project Handbook](https://www.eclipse.org/projects/handbook/#legaldoc).

## First thoughts

As with every artifact that you are publishing, it has to be maintained. So first, ask yourself, why you want to publish your images, how you want to deal with "older" images, how to deal with security vulnerabilities, etc.

We recommend to write down these policies, as they will help you remember what your duties are, and consumers will know what to expect.

## Base Images

If publishing images, you are responsible for the license compliance of all third party content. This can be hard for images, as there is an extremely large range of use cases and combinations of components.

Use a trustworthy base image, e.g. [Docker Official Images](https://docs.docker.com/trusted-content/official-images/). The Docker Official Images are a curated set of Docker repositories hosted on Docker Hub.

With one of these base images, you already have a curated image and the published package list, which you can use for your attributions.

We will follow this approach in this document. We are aware that this will not work for every use case, but it provides guidelines and at the very least a safe starting point.

**Recommended way:**

* Choose a trustworthy base image, e.g. [Docker Official Images](https://docs.docker.com/trusted-content/official-images/), that has published package scan results.
* Use this information for you attributions.
* Don't modify the base image (e.g. via apk update/upgrade), as this would change the published package list. If you need to do so, be prepared to update the published package list with your additions.

## Dockerfile

* Is stored in one of your Eclipse project repositories.
* Must have a license, normally the one you use for your project. Add a copyright / license header.
* See [DockerHub: Overview of best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

## Build process for images

* Publish a file (e.g. README) to DockerHub with information / annotations for your images.
* [Example GitHub action from Eclipse Tractus-X](https://eclipse-tractusx.github.io/docs/release/trg-4/trg-4-01#implementation).
* Recommended: add the legal documentation of your project to the image, also the dockerfile in a sub-folder (next to your code / artifact).
* Recommended: set up security scans for your images.

## Publish a notice for docker image

Due to legal constrains we need to annotate the published container images to make clear that we do our best to provide good images for demo purposes.

* Add a 'Notice for docker image' section to you README file or put the information in a separate file.
* Publish this file together with your image.

### Content

There are a few properties and links, that **must** be present on each notice, but they do vary for each image and product.
The minimum set of information is:

1. Information about your code.
1. Information about 3rd party components and the base image.
1. Information about the base image
1. General notice. 

If you are performing package scans for your image and publish the package list, add the link to your notice.

#### Information about your code

- Link to your project's home.
- Link to your project's repository.
- Direct link to the Dockerfile used to build the image.
- Link to the `LICENCE` file in the repository as 'Project License' and make clear, that this is the *project* licence, not the image license.
- Link to your image on the container registry (e.g. DockerHub).

Example from Eclipse Tractus-X:

```markdown
## Notice for Docker image

DockerHub: [https://hub.docker.com/r/tractusx/<your-image>](https://hub.docker.com/r/tractusx/<your-image>)

Eclipse Tractus-X product(s) installed within the image:

__<your product name>__

- GitHub: https://github.com/eclipse-tractusx/<your-product-repo>
- Project home: https://projects.eclipse.org/projects/automotive.tractusx
- Dockerfile: https://github.com/eclipse-tractusx/<your-product-repo>/blob/main/<path-to-Dockerfile>
- Project license: [Apache License, Version 2.0](https://github.com/eclipse-tractusx/<your-product-repo>/blob/main/LICENSE)
```

#### Information about 3rd-party components

***TODO***

#### Information about the base image

Links to:

* Docker image.
* Docker image project's home.
* Information about the image (e.g. package scans).
* Link to the Dockerfile.

Example from Eclipse Tractus-X:

```
**Used base image**

* [eclipse-temurin:20-jre-alpine](https://github.com/adoptium/containers)
* Official Eclipse Temurin DockerHub page: https://hub.docker.com/_/eclipse-temurin
* Eclipse Temurin Project: https://projects.eclipse.org/projects/adoptium.temurin
* Additional information about the Eclipse Temurin images: https://github.com/docker-library/repo-info/tree/master/repos/eclipse-temurin
```

#### General notice

Closing the notice, we provide a general statement about other tools potentially contained in the image and the demo/experimental purpose
of the images. This text was taken from Official Docker Images, e.g. Eclipse Temurin:

```markdown
As with all Docker images, these likely also contain other software which may be under other licenses
(such as Bash, etc. from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.
```

## Examples

* [Tractus-X Docker Images](https://hub.docker.com/u/tractusx)
* [Eclipse Temurin Docker Official Images](https://hub.docker.com/_/eclipse-temurin)

## Links

* [Official Docker Images](https://docs.docker.com/trusted-content/official-images/)
* [DockerHub: Overview of best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)

## Questions

* Delete old images
* Maintaining images for older versions
* Security scanning
* ...
