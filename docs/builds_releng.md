# Good practices for CI/CD Builds & Releng

* Security considerations: https://github.com/eclipse-cbi/best-practices/blob/main/software-supply-chain/osssc-best-practices.md
* CI/CD best practices:
  - https://wiki.eclipse.org/CI_best_practices
  - https://wiki.eclipse.org/Jenkins#CI_Best_Practices
  - https://wiki.eclipse.org/Jenkins#Jenkins_Pipeline_Best_Practices
  - https://wiki.eclipse.org/Jenkins#Build_performance_optimization
