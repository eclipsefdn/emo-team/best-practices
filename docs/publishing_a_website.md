
# Good practices to publish a website

* Have an introduction to open source for newcomers (aka onboarding).
* Provide information for contributors (aka developers).
* Optional - display the automated checks that are executed by the project.
* Provide links to relevant documentation (e.g. PMI, Eclipse project Handbook, Eclipse documentation, project guidelines..).

## Examples

* See [Eclipse Tractus-X website](https://eclipse-tractusx.github.io/).
  - Introduction for newcomers: https://eclipse-tractusx.github.io/docs/category/open-source-development
  - DevelopersHub: https://eclipse-tractusx.github.io/docs/developer
  - Relevant links: https://eclipse-tractusx.github.io/docs/dev_links
* See [Eclipse Temurin]().
