# Eclipse Project Best Practices


## About

This repository holds information about best practices that Eclipse projects can, and should, apply in their processes and repositories.

## What's in there?

* Documentation for [publishing your own Docker images](docs/publishing_docker_images.md).
* Recomendations for [building a website](docs/publishing_a_website.md).
* Good practices for [releng & CI/CD](docs/builds_releng.md).

## How does it work?

We are open to contributions. Feel free to [submit an issue](https://gitlab.eclipse.org/eclipsefdn/emo-team/best-practices/-/issues/new), or a pull request to amend the content.
